:PROPERTIES:
:ID:       d3c774a7-8f45-431f-a075-455b94ea6972
:END:
#+author: Yuval Langer
#+title: Comics Idea: A Typical Offtopic IRC Channel Experience.
#+options: toc:nil num:nil

0. A typical livingroom with a bunch of friends talking and whatnot.
1. Knock on the door. Everyone are looking at the door.
2. Someone opened the door. It's a guy with an origami paper hat and a
   stained smudged shirt, black beneath his eyes.
4. Walks in, poops on the floor, and writes walls of text on the
   walls.
5. One guy looks in disgust and another one says to the disgust guy
   "Just /ignore him."
6. Underneath is a caption written, "a typical offtopic IRC channel".
